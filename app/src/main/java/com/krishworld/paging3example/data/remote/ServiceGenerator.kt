import com.google.gson.GsonBuilder
import com.krishworld.paging3example.BuildConfig
import com.krishworld.paging3example.data.remote.ApiClient
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Krishan jangir on 05/06/2021.
 */

class ServiceGenerator {
    private var retrofit: Retrofit
    private val client: OkHttpClient
    val clientService: ApiClient

    init {
        //-----loggingInterceptor is used to log request and response information-----------
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpBuilder = OkHttpClient.Builder()

        //-----set timeout(1 min) for api request-----------
        okHttpBuilder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
            .readTimeout(60 * 1000, TimeUnit.MILLISECONDS)
            .writeTimeout(60 * 1000, TimeUnit.MILLISECONDS)

        okHttpBuilder.addInterceptor { chain ->
            val original = chain.request()
            val request: Request?

            //---------set api req header-------------
            request = original.newBuilder()
                .header("User-Agent", "Demo")
                .header("Accept", "application/Demo.v1.full+json")
                .header("Content-Type", "application/json")
                .method(original.method, original.body)
                .build()

            chain.proceed(request)
        }

        client = okHttpBuilder.build()
        retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(client)
            .addConverterFactory(
                GsonConverterFactory.create(
                    GsonBuilder()
                        .excludeFieldsWithoutExposeAnnotation().create()
                )
            ).build()

        clientService = retrofit.create(ApiClient::class.java)
    }

    companion object {
        //----------create instance of the ServiceGenerator class-----
        var instance: ServiceGenerator? = null
            get() {
                if (field == null) {
                    field = ServiceGenerator()
                }
                return field
            }
            private set
    }
}