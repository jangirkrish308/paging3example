package com.krishworld.paging3example.data.remote

import android.content.Context
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.krishworld.paging3example.data.paging.PhotosPagingSource

/**
 * Created by Krishan jangir on 05/06/2021.
 */
//----Repository is used to get network data----
class Repository(private val context: Context) {
    var api: ApiClient? = null

    init {
        api = ServiceGenerator.instance?.clientService
    }

    //----------return data from paging data source-----
    fun getAllImages() =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 100,
                enablePlaceholders = true
            ),
            pagingSourceFactory = { PhotosPagingSource(api!!) }
        ).liveData
}