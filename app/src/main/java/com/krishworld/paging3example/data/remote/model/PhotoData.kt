package com.krishworld.paging3example.data.remote.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
/**
 * Created by Krishan jangir on 05/06/2021.
 */
data class PhotoData(
    @SerializedName("id")
    @Expose
    var id: String? = null,

    @SerializedName("author")
    @Expose
    var author: String? = null,

    @SerializedName("width")
    @Expose
    var width: Long? = null,

    @SerializedName("height")
    @Expose
    var height: Long? = null,

    @SerializedName("url")
    @Expose
    var url: String? = null,

    @SerializedName("download_url")
    @Expose
    var downloadUrl: String? = null
)