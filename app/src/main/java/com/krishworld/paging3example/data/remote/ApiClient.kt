
package com.krishworld.paging3example.data.remote


import com.krishworld.paging3example.data.remote.model.PhotoData
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Krishan jangir on 05/06/2021.
 */

interface ApiClient {

    //------------For get photos based on page number--------
    @GET(ApiConfig.LIST)
    suspend fun getPhotos(
        @Query("page") page: Int?,
    ): List<PhotoData>

}