package com.krishworld.paging3example.data.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.krishworld.paging3example.data.remote.ApiClient
import com.krishworld.paging3example.data.remote.model.PhotoData
import retrofit2.HttpException
import java.io.IOException

/**
 * Created by Krishan jangir on 05/06/2021.
 */
//-----------Paging data source, return paged data based on requested page number-------
class PhotosPagingSource(private val apiClient: ApiClient) :
    PagingSource<Int, PhotoData>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PhotoData> {
        val position = params.key ?: 1
        return try {
            val response = apiClient.getPhotos(position)
            LoadResult.Page(
                data = response,
                prevKey = if (position == 1) null else position - 1,
                nextKey = if (response.isEmpty()) null else position + 1
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)

        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, PhotoData>): Int? {
        return state.anchorPosition
    }
}