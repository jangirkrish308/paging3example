package com.krishworld.paging3example.ui.home

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.krishworld.paging3example.R
import com.krishworld.paging3example.databinding.ActivityMainBinding
import com.krishworld.paging3example.ui.base.BaseBindingActivity
import com.krishworld.paging3example.ui.home.adapter.LoaderFooterAdapter
import com.krishworld.paging3example.ui.home.adapter.PhotosAdapter
import com.krishworld.paging3example.ui.home.viewmodel.MainViewModel
import com.krishworld.paging3example.utils.NetworkUtils

/**
 * Created by Krishan jangir on 05/06/2021.
 */
class MainActivity : BaseBindingActivity() {
    private var viewModel: MainViewModel? = null
    private var binding: ActivityMainBinding? = null
    var adapter: PhotosAdapter? = null

    //----for binding layout-------
    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding?.lifecycleOwner = this //----set lifecycle owner to binding--
        viewModel = ViewModelProvider(this, MainViewModel.Factory(this))
            .get(MainViewModel::class.java)
    }

    override fun createActivityObject(savedInstanceState: Bundle?) {
        mActivity = this
    }

    //----check internet connected or not-------
    override fun initialization() {
        adapter = PhotosAdapter()
        binding?.apply {
            photosRecycler.setHasFixedSize(true)
            photosRecycler.adapter = adapter!!.withLoadStateHeaderAndFooter(
                header = LoaderFooterAdapter { adapter!!.retry() },
                footer = LoaderFooterAdapter { adapter!!.retry() },
            )
        }

        //----check internet connection and if connected then call api -------
        if (NetworkUtils.isOnline(
                mActivity!!,
                true
            )
        ) {
            getImagesData()
        }
    }

    //----------get data from view model----
    private fun getImagesData() {
        viewModel?.getPhotos()?.observe(binding?.lifecycleOwner!!, Observer {
            adapter?.submitData(
                binding?.lifecycleOwner?.lifecycle!!,
                it
            )//--------add data to paging adapter
        })
    }
}