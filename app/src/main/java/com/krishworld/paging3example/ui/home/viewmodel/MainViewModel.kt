package com.krishworld.paging3example.ui.home.viewmodel

import android.content.Context
import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.krishworld.paging3example.data.remote.Repository
import com.krishworld.paging3example.data.remote.model.PhotoData


/**
 * Created by Krishan jangir on 05/06/2021.
 */
class MainViewModel(context: Context) : ViewModel() {
    private var repositry = Repository(context)

    //--------get photos from server and return liveData
    fun getPhotos(): LiveData<PagingData<PhotoData>> {
        return repositry.getAllImages().cachedIn(viewModelScope)
    }

    //--------factory for viewModel---
    class Factory(private val context: Context) : ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return MainViewModel(
                context
            ) as T
        }
    }
}