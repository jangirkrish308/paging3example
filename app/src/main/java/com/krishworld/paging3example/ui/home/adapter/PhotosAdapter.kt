package com.krishworld.paging3example.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.krishworld.paging3example.data.remote.model.PhotoData
import com.krishworld.paging3example.databinding.RowPhotosBinding

/**
 * Created by Krishan jangir on 05/06/2021.
 */
class PhotosAdapter :
    PagingDataAdapter<PhotoData, PhotosAdapter.PhotosViewHolder>(PHOTO_COMPARISION) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        val binding = RowPhotosBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PhotosViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        if (getItem(position) != null) {
            holder.bindTo(getItem(position)!!) //---bind view holder according to position---
        }
    }

    //-----------Photos ViewHolder class----------
    class PhotosViewHolder(private val rowPhotosBinding: RowPhotosBinding) :
        RecyclerView.ViewHolder(rowPhotosBinding.root) {
        fun bindTo(data: PhotoData) {
            rowPhotosBinding.model = data //--------bind data to ui--
        }
    }

    companion object {
        //------------compare data model item is same or not--------------
        private val PHOTO_COMPARISION = object : DiffUtil.ItemCallback<PhotoData>() {
            override fun areItemsTheSame(oldItem: PhotoData, newItem: PhotoData): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: PhotoData, newItem: PhotoData): Boolean =
                oldItem == newItem
        }
    }

}