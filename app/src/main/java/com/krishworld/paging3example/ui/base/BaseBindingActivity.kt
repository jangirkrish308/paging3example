package com.krishworld.paging3example.ui.base


import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity

/**
 * Created by Krishan jangir on 05/06/2021.
 */
abstract class BaseBindingActivity : AppCompatActivity() {
    var mActivity: AppCompatActivity? = null
    protected abstract fun setBinding()
    protected abstract fun createActivityObject(savedInstanceState: Bundle?)
    protected abstract fun initialization()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createActivityObject(savedInstanceState)
        setBinding()
        initialization()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}