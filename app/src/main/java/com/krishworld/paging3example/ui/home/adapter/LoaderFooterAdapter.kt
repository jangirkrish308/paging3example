package com.krishworld.paging3example.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krishworld.paging3example.databinding.RowLoaderFooterBinding

/**
 * Created by Krishan jangir on 05/06/2021.
 */
class LoaderFooterAdapter(private val retry: () -> Unit) :
    LoadStateAdapter<LoaderFooterAdapter.LoaderFooterViewHolder>() {


    override fun onBindViewHolder(holder: LoaderFooterViewHolder, loadState: LoadState) {
        holder.bindTo(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoaderFooterViewHolder {
        val binding =
            RowLoaderFooterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoaderFooterViewHolder(binding)
    }

    inner class LoaderFooterViewHolder(private val rowLoaderFooterBinding: RowLoaderFooterBinding) :
        RecyclerView.ViewHolder(rowLoaderFooterBinding.root) {
        init {
            rowLoaderFooterBinding.btnRetry.setOnClickListener {
                retry.invoke()
            }
        }

        fun bindTo(loadState: LoadState) {
            rowLoaderFooterBinding.apply {
                btnRetry.isVisible = loadState !is LoadState.Loading
            }
        }
    }
}