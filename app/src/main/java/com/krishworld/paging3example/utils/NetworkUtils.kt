package com.krishworld.paging3example.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Gravity
import android.widget.Toast
import com.krishworld.paging3example.R

/**
 * Created by Krishan jangir on 04/06/2021.
 */

object NetworkUtils {

    //check internet is available or not
    @Suppress("DEPRECATION")
    fun isOnline(context: Context, showToast: Boolean): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork
            val actNw = connectivityManager.getNetworkCapabilities(nw)
            if (nw == null || actNw == null) {
                if (showToast) showToast(
                    context,
                    context.getString(R.string.no_internet_connection)
                )
                return false
            }

            val isConnected = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                //for other device how are able to connect with Ethernet
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                //for check internet over Bluetooth
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
                else -> false
            }
            return isConnected
        } else {
            val isConnected = connectivityManager.activeNetworkInfo?.isConnected ?: false
            if (!isConnected && showToast) showToast(
                context,
                context.getString(R.string.no_internet_connection)
            )
            return isConnected
        }
    }

    //to show toast
    private fun showToast(context: Context, msg: String) {
        val toast = Toast.makeText(context, msg, Toast.LENGTH_LONG)
        toast.setGravity(Gravity.BOTTOM, 0, 0)
        toast.show()
    }

}