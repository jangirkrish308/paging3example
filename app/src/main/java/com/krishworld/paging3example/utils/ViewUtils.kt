package com.krishworld.paging3example.utils

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.facebook.shimmer.Shimmer
import com.facebook.shimmer.ShimmerDrawable
import com.krishworld.paging3example.R

object ViewUtils {
    @JvmStatic
    @BindingAdapter("image")
    fun loadImageIntoImageView(view: ImageView, ProfilePhoto: String?) {
        if (ProfilePhoto != null) {
            loadImage(view, ProfilePhoto)
        }
    }

    //------load image in ImageView------
    private fun loadImage(view: ImageView, imageUrl: String?) {
        Glide.with(view.context)
            .load(imageUrl)
            .apply(RequestOptions().override(200, 200))
            .placeholder(shimmerDrawable)
            .into(view)
    }

    // This is the shimmer placeholder for the imageView
    private val shimmer =
        Shimmer.AlphaHighlightBuilder()// The attributes for a ShimmerDrawable is set by this builder
            .setDuration(1500) // how long the shimmering animation takes to do one full sweep
            .setBaseAlpha(0.6f) //the alpha of the underlying children
            .setHighlightAlpha(0.5f) // the shimmer alpha amount
            .setDirection(Shimmer.Direction.LEFT_TO_RIGHT)
            .setAutoStart(true)
            .build()

    // This is the placeholder for the imageView
    private val shimmerDrawable = ShimmerDrawable().apply {
        setShimmer(shimmer)
    }
}